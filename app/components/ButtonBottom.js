import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View, 
} from 'react-native'

import React, { Component } from 'react'

import { wp, width, height }  from '../utils'

class ButtonBottom extends Component {
 constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity activeOpacity={0.75} style={styles.buttonSave} onPress={this.props.onPress}>
        <Image style={styles.buttonImage} source={this.props.image}/>
        <Text style={styles.buttonText}>{this.props.text}</Text>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  buttonSave: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonImage: {
    width: wp(100),
    height: wp(14)
  },
  buttonText: {
    position: 'absolute',
    color: '#fff',
    top: wp(4),
    textAlign: 'center',
    fontFamily: 'nunito-regular',
    fontSize: 18,
  },
})

export default ButtonBottom
