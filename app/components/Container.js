import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: '500',
  },
});

const Container = ({ backgroundColor, onPress, children, text }) => (
  <View style={[styles.container, { backgroundColor }]}>
      <Text style={styles.text}>{text ? text : backgroundColor}</Text>
    {children}
  </View>
);

export default Container;
