import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { wp } from '../utils'

class HeaderBar extends Component {
  render() {
    //const taglineStyle = this.props.taglineColor ? {color: this.props.taglineColor} : {}
    return (
      <View sytle={styles.wrapper}>
        <Text style={styles.title}>{this.props.title}</Text>
        <Text style={[styles.tagline, this.props.taglineStyle]}>{this.props.tagline}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
      justifyContent: 'center',
      alignItems: 'center'
  },
  title: {
    fontFamily: 'nunito-bold',
    fontSize: 20,
    color: '#000',
    textAlign: 'center',
  },
  tagline: {
    textAlign: 'center',
    fontFamily: 'nunito-regular',
    color:'#000',
  }
})


export default HeaderBar
