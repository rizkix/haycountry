import {
  StyleSheet,
  TouchableOpacity,
  Text,
  TextInput,
  Image,
  View, 
  ScrollView, 
  Dimensions,
} from 'react-native'

import React, { Component } from 'react'

import { wp, width, height }  from '../utils'

class FormInput extends Component {
 constructor(props) {
    super(props);
    this.state = { 
      borderColor: '#d3d3d3', 
    };
   this.onChangeText = this.onChangeText.bind(this)
  }
  onFocus() {
    this.setState({
      borderColor: '#6fc842'
    })
  }
  onBlur() {
    this.setState({
      borderColor: '#d3d3d3'
    })
  }

  onChangeText(text) {
    if (this.props.onChangeText) {
      this.props.onChangeText(text)
    }
  }

  render() {
    const borderColor = this.props.checked ? "#6fc842" : this.state.borderColor
    const labelColor = this.props.checked ? "#6fc842" : "#8c8c8c"

    return (
      <View>
        <View style={styles.labelWrapper}>
          <Text style={[styles.label, {color: labelColor}]}>{this.props.label}</Text>
          {
            this.props.checked &&
              <Image style={styles.checkImage} source={require('../images/submit/icon-success.png')}/>
            
          }
        </View>
        <TextInput
          underlineColorAndroid='rgba(255,255,255,0)'
          placeholderTextColor="#000"
          placeholder={this.props.placeholder}
          style={[styles.input, {borderBottomColor: borderColor}, this.props.customColor]}
          onChangeText={(text) => {this.onChangeText(text)}}
          secureTextEntry={this.props.password ? true : false}
          value={this.props.value}
          onBlur={ () => this.onBlur() }
          onFocus={ () => this.onFocus() }
          editable={this.props.editable}
        />
        {this.props.children}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  labelWrapper: {
    flexDirection: 'row', 
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 0,
    paddingBottom: 0,
  },
  label: {
    fontFamily: 'nunito-bold',
    fontSize:14,
  },
  checkImage: {
    marginLeft: 5,
    width: 10,
    height: 10,
  },
  input: {
    marginTop: 0,
    paddingTop: 0,
    marginLeft: 0,
    paddingLeft: 0,
    paddingBottom: 14,
    borderBottomColor: '#d3d3d3',
    borderBottomWidth: 1,
    textAlign: 'left',
    fontSize: 19,
    color:'#000',
    fontFamily: 'nunito-regular',
  },
})

export default FormInput
