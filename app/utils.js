import { Dimensions } from 'react-native'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export const wp = (percentage) => {
  const value = (percentage * width) / 100;
  return Math.round(value);
}

