import React, { Component } from 'react';
import FormInput from '../components/FormInput';
import ButtonBottom from '../components/ButtonBottom';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  TextInput,
  View, 
  ScrollView, 
  Button,
  Dimensions,
} from 'react-native'

import { wp, width, height }  from '../utils'

import { NavigationActions } from 'react-navigation';
const GoToListing = NavigationActions.navigate({
  routeName: 'HOME',
  params: {},
  action: NavigationActions.navigate({ routeName: 'LISTING' }),
});


class Box extends Component {
  render() {
    const data = this.props.data
    return (
      <View style={styles.box}>
        <FormInput label="Keywords" value={data.keywords} customColor={{color:'#2d74cf'}} editable={false} />
        <FormInput label="Category" value={data.category} editable={false}/>
        <FormInput label="Location (50 km from you)" value={data.location} editable={false}/>

        <View style={styles.boxBottom}>
          <TouchableOpacity activeOpacity={0.75} style={styles.buttonDelete}>
            <Image style={styles.deleteImage} source={require("../images/saved_searches/btn-delete.png")}/>
          </TouchableOpacity>

          <TouchableOpacity activeOpacity={0.75} style={styles.buttonSearch} onPress={this.props.onPress}>
            <Image style={styles.searchImage} source={require("../images/saved_searches/btn-green-round-sm-03.png")}/>
            <Text style={styles.searchText}>Search</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}


class Screen extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    const data= [
      {keywords: 'Race Horses', category: 'Horses', location: 'Vilnius, Lithuania'},
      {keywords: 'Race Horses', category: 'Horses', location: 'Vilnius, Lithuania'},
      {keywords: 'Race Horses', category: 'Horses', location: 'Vilnius, Lithuania'},
    ]
    return (
      <View style={styles.container}>
          {
            data.map((d, i) => {
              return <Box data={d} key={i} onPress={this.props.goToExploreListing} />
            })
          }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center'
  },
  box: {
    //overflow:'hidden',
    marginTop:15,
    marginBottom:25,
    marginLeft: wp(6.66667),
    marginRight: wp(6.66667),
    backgroundColor: '#fff',
    elevation: 10,
    width: wp(85.86667),
    borderRadius:8,
    paddingTop:15,
    paddingBottom:25,
    paddingLeft: 25,
    paddingRight: 25,

    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 3,
  },
  boxBottom: {
    marginTop: 25,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  buttonDelete: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  deleteImage: {
    width: wp(6.6667),
    height: wp(7.444467)
  },
  buttonSearch: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  searchImage: {
    width: wp(30),
    height: wp(12.8)
  },
  searchText: {
    position: 'absolute',
    textAlign: 'center',
    fontFamily: 'nunito-regular',
    fontSize: 15,
    color:'#fff'
  },
})

export default Screen
