import React, { Component } from 'react';
import HeaderBar from '../components/HeaderBar';
import FormInput from '../components/FormInput';
import ButtonBottom from '../components/ButtonBottom';

import { HeaderBackButton } from 'react-navigation';

import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View, 
  ScrollView, 
  Button,
  Dimensions,
} from 'react-native'

import { wp, width, height }  from '../utils'
import { headerStyle } from '../styles'

class Screen extends Component {
  static navigationOptions = ({navigation}) => ({
    headerTitle: 
    <View style={{justifyContent:'center', alignItems:'center', alignSelf: 'center', flexGrow: 1}}>
      <HeaderBar title='New Listing' tagline="Step 2 of 4"/>
      </View>,
    headerStyle: headerStyle,
    headerTitleStyle: {alignSelf: 'center'},
    headerLeft: <HeaderBackButton onPress={() => navigation.goBack(null)} />,
    headerRight: <View/>
  })

  constructor(props) {
    super(props)
    this.state = {
      name: 'Modestas Baranauskas',
      phone: '+370 615 7752',
      email: 'Baran.modestas@gmail.com',

      fillComplete: true

    }
  }

  render() {
    const buttonText = this.state.fillComplete ? "Move To Step 3" : "Fill All Fields To Move To Step 3"
    const buttonImage = this.state.fillComplete ? require('../images/contact_seller/btn-green.png') : require('../images/submit/btn-grey.png') 


    return (
      <View style={styles.container}>
        <ScrollView style={styles.wrapper}>
          <View style={{flexDirection: 'row', alignItems: 'center', marginTop:40, marginBottom:20}}>
            <Image style={styles.infoImage} source={require('../images/submit/icon-phone.png')}/>
            <Text style={styles.header}>CONTACT INFO</Text>
          </View>
          <FormInput label="Your Name" value={this.state.name} checked={true}/>
          <FormInput  label="Your Phone" value={this.state.phone} checked={true}/>
          <FormInput  label="Your Email" value={this.state.email} checked={true}/>
        </ScrollView>

        <ButtonBottom text={buttonText} image={buttonImage} onPress={() => this.props.navigation.navigate("STEP3")}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  wrapper: {
    paddingLeft:wp(6.66667),
    paddingRight:wp(6.66667),
  },
  header: {
    marginLeft: 10,
    color: '#256fcd',
    fontFamily: 'nunito-regular',
    fontSize: 16,
  },
  infoImage: {
    width: 16,
    height: 16,
  }
})

export default Screen
