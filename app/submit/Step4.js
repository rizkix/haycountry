import React, { Component } from 'react';
import HeaderBar from '../components/HeaderBar';
import FormInput from '../components/FormInput';
import ButtonBottom from '../components/ButtonBottom';

import { HeaderBackButton } from 'react-navigation';

import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View, 
  ScrollView, 
  Button,
  Dimensions,
} from 'react-native'

import { wp, width, height }  from '../utils'

import { headerStyle } from '../styles'

import { CameraKitCamera, CameraKitCameraScreen } from 'react-native-camera-kit'
import Modal from 'react-native-modal';


class ImageChooser extends Component {
  render() {
    const checkImage ={
      active: require('../images/submit/icon-checkbox_active.png'),
      inactive: require('../images/submit/icon-checkbox.png')
    }
    const activeImage = this.props.active
    return (
      <View style={styles.chooserWrapper}>
        {
          this.props.images.map((d, i) => {
            const checkImageButton = activeImage === i ? checkImage.active : checkImage.inactive
            return (
              <TouchableOpacity activeOpacity={0.75} style={styles.imageWrap} key={i} onPress={() => this.props.onChangeImage(i)}>
                <Image style={styles.image} source={d.url}/>
                <Image style={styles.checkImage} source={checkImageButton}/>
              </TouchableOpacity>
            )
          })
        }
      </View>
    )
  }
}

const images = [
  {
    url: require('../images/submit/img-10.jpg'),
  },
  {
    url: require('../images/submit/img-11.jpg'),
  },
  {
    url: require('../images/submit/img-12.jpg'),
  },
  {
    url: require('../images/submit/img-13.jpg'),
  }
]

class Screen extends Component {
  static navigationOptions = ({navigation}) => ({
    headerTitle: 
   <View style={{justifyContent:'center', alignItems:'center', alignSelf: 'center', flexGrow: 1}}>
<HeaderBar title='New Listing' tagline="Final Step!" taglineStyle={{fontFamily: 'nunito-bold', color:"#5fa63f"}}/>
      </View>,
    headerStyle: headerStyle,
    headerTitleStyle: {alignSelf: 'center'},
    headerLeft: <HeaderBackButton onPress={() => navigation.goBack(null)} />,
    headerRight: <View/>
  })

  constructor(props) {
    super(props)
    this.state = {
      activeImage: 1,

      fillComplete: true,
      isModalVisible: false

    }

    this.onChangeImage = this.onChangeImage.bind(this)
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.onBottomButtonPressed = this.onBottomButtonPressed.bind(this)
  }

  closeModal() {
    this.setState({isModalVisible: false})
  }

  openModal() {
    this.setState({isModalVisible: true})
  }

  onChangeImage(key) {
    this.setState({activeImage: key})
  }

  onBottomButtonPressed(event) {
    const captureImages = JSON.stringify(event.captureImages);
    alert(
      `${event.type} button pressed`,
      `${captureImages}`,
      [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false }
    )
  }

  render() {
    const buttonText = this.state.fillComplete ? "Submit Listing" : "Select Image To Submit Listing"
    const buttonImage = this.state.fillComplete ? require('../images/contact_seller/btn-green.png') : require('../images/submit/btn-grey.png') 




    return (
      <View style={styles.container}>

        <Modal isVisible={this.state.isModalVisible} style={styles.modalContent} useNativeDriver={true} onBackButtonPress={this.closeModal} onModalShow={this.props.onModalShow} onModalHide={this.props.onModalShow}>
<CameraKitCameraScreen
        actions={{ rightButtonText: 'Done', leftButtonText: 'Cancel' }}
        onBottomButtonPressed={(event) => this.onBottomButtonPressed(event)}
        flashImages={{
          on: require('../images/camera/flashOn.png'),
          off: require('../images/camera/flashOff.png'),
          auto: require('../images/camera/flashAuto.png')
        }}
        cameraFlipImage={require('../images/camera/cameraFlipIcon.png')}
        captureButtonImage={require('../images/camera/cameraButton.png')}
        style={{
          flex: 1,
          backgroundColor: 'white'
        }}
        cameraOptions={{
          flashMode: 'auto',             // on/off/auto(default)
          focusMode: 'on',               // off/on(default)
          zoomMode: 'on',                // off/on(default)
          ratioOverlay:'1:1',            // optional, ratio overlay on the camera and crop the image seamlessly
          ratioOverlayColor: '#00000077' // optional
        }}
      />
    </Modal>
        <ScrollView style={styles.wrapper}>
          <View style={{marginBottom:50}}>
            <View style={{alignItems: 'center'}}>
              <TouchableOpacity activeOpacity={0.75} style={styles.buttonCamera} onPress={this.openModal}>
                <Text style={styles.takePhotoText}>Take Photos With Camera</Text>
              </TouchableOpacity>
            </View>
            <View>
              <ImageChooser images={images} active={this.state.activeImage} onChangeImage={this.onChangeImage} />
            </View>
            <Text style={styles.nomore}>No More Photos</Text>
          </View>
        </ScrollView>
        <ButtonBottom text={buttonText} image={buttonImage} onPress={() => this.props.navigation.navigate("HOME")}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalContent: {
    margin:0,
    padding:0,
    width: "100%",
    height: "100%"
  },
  wrapper: {
    width: "100%",
    paddingLeft:wp(6.66667),
    paddingRight:wp(6.66667),
  },
  buttonCamera: {
    marginTop: 30,
    padding: 13,
    width: wp(72),
    borderRadius: 4,
    borderColor: '#cdcdcd',
    borderWidth: 1
  },
  takePhotoText: {
    color: '#090909',
    textAlign:'center',
    fontFamily: 'nunito-regular',
    fontSize: 16,
  },
  nomore: {
    marginTop:20,
    color: '#c5c5c5',
    textAlign:'center',
    fontFamily: 'nunito-regular',
    fontSize: 18,
  },
  chooserWrapper: {
    flex:1,
    marginTop: 40,
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    flexDirection: 'row'
  },
  imageWrap: {
    overflow: 'hidden',
  },
  image: {
    marginBottom: wp(3),
    borderRadius:10,
    width: wp(42),
    height: wp(42),
  },
  checkImage: {
    position: 'absolute',
    top: 10,
    right: 10,
    width: 30,
    height: 30
  }
})

export default Screen
