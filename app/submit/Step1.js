import React, { Component } from 'react';
import HeaderBar from '../components/HeaderBar';
import FormInput from '../components/FormInput';
import ButtonBottom from '../components/ButtonBottom';

import { HeaderBackButton } from 'react-navigation';

import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View, 
  ScrollView, 
  Button,
  Dimensions,
} from 'react-native'

import { wp, width, height }  from '../utils'
import { headerStyle } from '../styles'

class Screen extends Component {
  static navigationOptions = ({navigation}) => ({
    headerTitle: 
    <View style={{justifyContent:'center', alignItems:'center', alignSelf: 'center', flexGrow: 1}}>
      <HeaderBar title='New Listing' tagline="Step 1 of 4"/>
      </View>,
    headerStyle: headerStyle,
    headerTitleStyle: {alignSelf: 'center'},
    headerLeft: <HeaderBackButton onPress={() => navigation.goBack(null)} />,
    headerRight: <View/>
  })

  constructor(props) {
    super(props)
    this.state = {
      listingName: 'Alfafa Hay For Sale',
      category: 'Hay',
      availability: '1.567',
      type: 'Type',
      fertilized: 'Fertilized?',
      description: 'Description',

      fillComplete: false

    }
  }

  render() {
    const buttonText = this.state.fillComplete ? "Move To Step 2" : "Fill All Fields To Move To Step 2"
    const buttonImage = this.state.fillComplete ? require('../images/contact_seller/btn-green.png') : require('../images/submit/btn-grey.png') 


    return (
      <View style={styles.container}>
        <ScrollView style={styles.wrapper}>
          <View style={{marginBottom:50}}>
            <View style={{flexDirection: 'row', alignItems: 'center', marginTop:40, marginBottom:20}}>
              <Image style={styles.infoImage} source={require('../images/submit/icon-info.png')}/>
              <Text style={styles.header}>PROFILE INFO</Text>
            </View>
            <FormInput onChangeText={(text)=>{this.setState({listingName: text})}} label="Listing Name" value={this.state.listingName} checked={true}/>
            <FormInput onChangeText={(text)=>{this.setState({category: text})}} label="Category" value={this.state.category} checked={true}/>
            <FormInput onChangeText={(text)=>{this.setState({availability: text})}} label="Availability" value={this.state.availability} checked={true}/>
            <FormInput onChangeText={(text)=>{this.setState({type: text})}} label="Type" value={this.state.type}/>
            <FormInput onChangeText={(text)=>{this.setState({fertilized: text})}} label="Fertilized" value={this.state.fertilized}/>
            <FormInput onChangeText={(text)=>{this.setState({description: text})}} label="Description" value={this.state.description}/>
          </View>
        </ScrollView>

        <ButtonBottom text={buttonText} image={buttonImage} onPress={() => this.props.navigation.navigate("STEP2")}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  wrapper: {
    paddingLeft:wp(6.66667),
    paddingRight:wp(6.66667),
  },
  header: {
    marginLeft: 10,
    color: '#256fcd',
    fontFamily: 'nunito-regular',
    fontSize: 16,
  },
  infoImage: {
    width: 16,
    height: 16,
  }
})

export default Screen
