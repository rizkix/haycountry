import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View, 
  ScrollView, 
  Dimensions,
} from 'react-native'

import { wp, width, height }  from '../utils'
import { NavigationActions } from 'react-navigation';

class Menu extends Component {
  render() {
    const lists = {
      billing: {
        text: 'Billing Info',
        icon: require("../images/profile/icon-credit-card.png"),
        screen: 'BILLING'
      },
      edit: {
        text: 'Edit Profile',
        icon: require("../images/profile/icon-edit.png"),
        screen: 'EDIT_PROFILE'
      },
      list: {
        text: 'My Listings',
        icon: require("../images/profile/icon-list.png"),
        screen: 'LIST'
      },
      notification: {
        text: 'Notification Settings',
        icon: require("../images/profile/icon-notification.png"),
        screen: 'NOTIFICATION'
      }
    }
    const menuImage = lists[this.props.obj].icon
    const menuText = lists[this.props.obj].text
    const menuScreen = lists[this.props.obj].screen


    return (
      <TouchableOpacity activeOpacity={0.75} style={this.props.style ? this.props.style : styles.buttonMenu} onPress={this.props.onPress}>
        <Image style={styles.menuImage} source={menuImage}/>
        <Text style={styles.menuText}>{menuText}</Text>
        <Image style={styles.menuArrow} source={require('../images/profile/icon-right.png')}/>
      </TouchableOpacity>
    )
  }
}


const navigateAction = NavigationActions.navigate({
  routeName: 'HOME',
  params: {},
  action: NavigationActions.navigate({ routeName: 'LISTING' }),
});

const resetAction = NavigationActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'App' })],
});

class Screen extends Component {
  static navigationOptions = {
    header: null
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.wrapper}>

          <Text style={styles.topUsername}>baran.modestas</Text>
          <Text style={styles.header1}>Your active plan:</Text>

          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={styles.greenText}>Professional</Text>
            <Text style={styles.price}>($7.99/mo)</Text>
          </View>

          <Text style={styles.description}>
            <Text>You can upload </Text>
            <Text style={styles.descriptionBold}>2</Text>
            <Text numberOfLines={2}> more listings. Upgrade if you need more</Text>
          </Text>
          <Text style={styles.expiration}>3 days left</Text>

          <View style={styles.buttonGroup}>
            <TouchableOpacity activeOpacity={0.75} style={styles.buttonOutline} onPress={() => this.props.navigation.navigate('PLAN_UPGRADE')}>
              <Text style={styles.upgradeText}>Upgrade</Text>
            </TouchableOpacity>

            <TouchableOpacity activeOpacity={0.75} style={[styles.buttonRenew, {marginLeft:18}]} onPress={() => this.props.navigation.dispatch(resetAction)}>
              <Image resizeMode="cover" style={styles.renewImage} source={require("../images/profile/btn-green-round-sm-02.png")}/>
              <Text style={styles.renewText}>Renew Plan</Text>
            </TouchableOpacity>
          </View>

          <View style={{marginTop: 20, marginBottom: 20}}>
            <Menu onPress={() => this.props.navigation.navigate("EDIT_PROFILE")} style={styles.buttonMenuTop} obj="edit"/>
            <Menu onPress={() => this.props.navigation.navigate("BILLING_INFO")} obj="billing"/>
            <Menu onPress={() => this.props.navigation.navigate("MY_LISTINGS")} obj="list"/>
            <Menu onPress={() => this.props.navigation.navigate("NOTIFICATION")} obj="notification"/>
          </View>

          <View style={{alignItems: 'center'}}>
            <TouchableOpacity activeOpacity={0.75} style={styles.buttonSignout} onPress={() => this.props.navigation.dispatch(navigateAction)}>
              <View style={styles.buttonSignoutInner}>
                <Image style={styles.signoutImage} source={require("../images/profile/icon-sign-out.png")}/>
                <Text style={styles.signoutText}>Log Out</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  wrapper: {
    paddingLeft:wp(6.66667),
    paddingRight:wp(6.66667)
  },
  topUsername: {
    textAlign: 'left',
    color: '#000',
    fontFamily: 'nunito-bold',
    fontSize: 26,
    marginTop: 50,
    marginBottom: 30
  },
  header1: {
    textAlign: 'left',
    color: '#1a1a1a',
    fontFamily: 'nunito-regular',
    fontSize: 13,
  },
  greenText: {
    color: '#53a235',
    fontSize: 26,
    fontFamily: 'nunito-bold',
  },
  price: {
    color: '#868686',
    fontSize: 20,
    fontFamily: 'nunito-regular',
    marginLeft: 5,
    paddingTop: 2
  },
  description: {
    color: '#000',
    fontSize: 16,
    lineHeight: 24,
    fontFamily: 'nunito-regular',
    marginTop: 13,
    marginBottom: 15,
  },
  descriptionBold: {
    fontFamily: 'nunito-bold',
  },
  expiration: {
    color: '#e23131',
    fontSize: 15,
    fontFamily: 'nunito-regular',
    marginBottom: 15,
  },
  buttonGroup: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 40,
  },
  buttonOutline:{
    width: wp(34.93334),
    height: wp(16.13334),
    backgroundColor:'#fff',
    borderRadius:wp(1.2),
    borderWidth: 1,
    borderColor: '#339b30',
    justifyContent: 'center',
    alignItems: 'center',
  },
  upgradeText: {
    position: 'absolute',
    color: '#000',
    top: wp(4.5),
    textAlign: 'center',
    fontFamily: 'nunito-regular',
    fontSize: 19,
  },
  buttonRenew: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  renewImage: {
    width: wp(45.86667),
    height: wp(16.13334)
  },
  renewText: {
    position: 'absolute',
    color: '#fff',
    top: wp(4.5),
    textAlign: 'center',
    fontFamily: 'nunito-regular',
    fontSize: 19,
  },
  buttonMenuTop: {
    borderBottomColor: '#d3d3d3',
    borderBottomWidth: 1,
    borderTopColor: '#d3d3d3',
    borderTopWidth: 1,
    height: wp(20),
    alignItems: 'center',
    flexDirection: 'row',
    width: wp(86.66666),
  },
  buttonMenu: {
    borderBottomColor: '#d3d3d3',
    borderBottomWidth: 1,
    height: wp(20),
    alignItems: 'center',
    flexDirection: 'row',
    width: wp(86.66666),
  },
  menuImage: {
    width: 30,
    height: 30
  },
  menuArrow: {
    position: 'absolute',
    right: '5%',
    width: 24,
    height: 24
  },
  menuText: {
    color: '#000',
    fontFamily: 'nunito-bold',
    fontSize: 18,
    marginLeft:17
  },
  buttonSignout: {
    marginTop: 10,
    marginBottom: 50,
    flexDirection: 'row', 
    alignItems: 'center',
    justifyContent: 'center',
    width: 160,
    paddingTop:10,
    paddingBottom:10,
  },
  buttonSignoutInner: {
    flexDirection: 'row', 
    alignItems: 'center',
    justifyContent: 'center'
  },
  signoutImage: {
    width: 24,
    height: 24
  },
  signoutText: {
    fontFamily: 'nunito-bold',
    fontSize: 18,
    marginLeft: 12,
    color: '#959494'
  }
})

export default Screen;
