import React, { Component } from 'react';
import HeaderBar from '../components/HeaderBar';
import ButtonBottom from '../components/ButtonBottom';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View, 
  ScrollView, 
} from 'react-native'

import { wp, width, height }  from '../utils'

class Item extends Component {
  render() {
    const checkButton = {
      active: require('../images/radio_active.png'),
      inactive: require('../images/radio.png'),
    }

    const checkImage = this.props.active ? checkButton.active : checkButton.inactive

    return (
      <View style={[styles.menuWrapper, this.props.menuStyle]}>
        <Text style={styles.checkText}>{this.props.text}</Text>
        <TouchableOpacity activeOpacity={0.75} style={styles.buttonCheck} onPress={this.props.onPress}>
          <Image style={styles.checkImage} source={checkImage}/>
        </TouchableOpacity>
      </View>
    )
  }
}



import { headerStyle } from '../styles';
class Screen extends Component {
  static navigationOptions = {
    headerTitle: <View style={{justifyContent:'center', alignItems:'center', alignSelf: 'center', flexGrow: 1}}>
      <HeaderBar title='Notifications'/>
      </View>,
    headerStyle: headerStyle,
    headerRight: <View/>
  }

  constructor(props) {
    super(props)
    this.state = {
      remindEmail: true,
      remindTextMessage:false,
      remindPushNotifications: true,

      limitedEmail: true,
      limitedTextMessage: false,
      limitedPushNotification: true,
    }
    this.toggleRemindEmail = this.toggleRemindEmail.bind(this)
    this.toggleRemindTextMessage = this.toggleRemindTextMessage.bind(this)
    this.toggleRemindPushNotifications = this.toggleRemindPushNotifications.bind(this)

    this.toggleLimitedEmail = this.toggleLimitedEmail.bind(this)
    this.toggleLimitedTextMessage = this.toggleLimitedTextMessage.bind(this)
    this.toggleLimitedPushNotifications = this.toggleLimitedPushNotifications.bind(this)
  
  }

  toggleRemindEmail(menu) {
    this.setState({remindEmail: !this.state.remindEmail})
  }

  toggleRemindTextMessage(menu) {
    this.setState({remindTextMessage: !this.state.remindTextMessage})
  }

  toggleRemindPushNotifications(menu) {
    this.setState({remindPushNotifications: !this.state.remindPushNotifications})
  }

  toggleLimitedEmail(menu) {
    this.setState({limitedEmail: !this.state.limitedEmail})
  }

  toggleLimitedTextMessage(menu) {
    this.setState({limitedTextMessage: !this.state.limitedTextMessage})
  }

  toggleLimitedPushNotifications(menu) {
    this.setState({limitedPushNotifications: !this.state.limitedPushNotifications})
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.wrapper}>
          <Text style={styles.header}>REMIND NOTIFICATIONS</Text>
          <Text style={styles.lead}>Get notification about services expiration dates</Text>

          <View style={{marginBottom:10}}>
            <Item onPress={this.toggleRemindEmail} text="Email" active={this.state.remindEmail} menuStyle={{borderTopWidth: 1, borderTopColor: '#d3d3d3'}}/>
            <Item onPress={this.toggleRemindTextMessage} text="Text Messages" active={this.state.remindTextMessage}/>
            <Item onPress={this.toggleRemindPushNotifications} text="Push Notifications" active={this.state.remindPushNotifications}/>
          </View>

          <Text style={styles.header}>LIMITED TIME OFFERS</Text>
          <Text style={styles.lead}>Get notification special limited offers, discounts</Text>

          <View style={{marginBottom:50}}>
            <Item onPress={this.toggleLimitedEmail} text="Email" active={this.state.limitedEmail} menuStyle={{borderTopWidth: 1, borderTopColor: '#d3d3d3'}}/>
            <Item onPress={this.toggleLimitedTextMessage} text="Text Messages" active={this.state.limitedTextMessage}/>
            <Item onPress={this.toggleLimitedPushNotifications} text="Push Notifications" active={this.state.limitedPushNotifications}/>
          </View>

        </ScrollView>

        <ButtonBottom text="Save Changes" image={require('../images/billing_info/btn-blue.png')} onPress={() => this.props.navigation.goBack()}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  wrapper: {
    paddingLeft:wp(6.66667),
    paddingRight:wp(6.66667),
  },
  header: {
    marginTop: 35,
    marginBottom: 5,
    color: '#000',
    fontFamily: 'nunito-regular',
    fontSize: 16,
    paddingRight:wp(6.66667),
  },
  lead: {
    marginBottom: 30,
    color: '#5e5e5e',
    fontFamily: 'nunito-regular',
    fontSize: 14,
    paddingRight:wp(6.66667),
    lineHeight:20,
  },
  menuWrapper: {
    borderBottomColor: '#d3d3d3',
    borderBottomWidth: 1,
    height: wp(20),
    alignItems: 'center',
    flexDirection: 'row',
    width: wp(86.66666),
  },
  buttonCheck: {
    position: 'absolute',
    right: 0
  },
  checkText: {
    textAlign: 'left',
    fontFamily: 'nunito-bold',
    fontSize: 18,
    color:'#000'
  },
  checkImage: {
    width: wp(19.4),
    height: wp(10.5)
  },
})

export default Screen
