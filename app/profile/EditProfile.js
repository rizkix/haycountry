import React, { Component } from 'react';
import HeaderBar from '../components/HeaderBar';
import FormInput from '../components/FormInput';
import ButtonBottom from '../components/ButtonBottom';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View, 
  ScrollView, 
  Button,
  Dimensions,
} from 'react-native'

import { wp, width, height }  from '../utils'



import { headerStyle } from '../styles';
class Screen extends Component {
  static navigationOptions = {
    headerTitle: <View style={{justifyContent:'center', alignItems:'center', alignSelf: 'center', flexGrow: 1}}>
      <HeaderBar title='Edit Profile'/>
      </View>,
    headerStyle: headerStyle,
    headerRight: <View/>
  }

  constructor(props) {
    super(props)
    this.state = {
      firstName: 'Modestas',
      lastName: 'Baranauskas',
      email: 'Baran.modestas@gmail.com',
      password: 'Baran.modestas@gmail.com',
      repeatPassword: 'Baran.modestas@gmail.com'
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.wrapper}>
          <View style={{marginBottom:50}}>
            <Text style={styles.header}>PROFILE INFO</Text>
            <FormInput onChangeText={(text) => {this.setState({firstName: text})}} label="First Name" value={this.state.firstName}/>
            <FormInput onChangeText={(text) => {this.setState({lastName: text})}} label="Last Name" value={this.state.lastName}/>
            <FormInput onChangeText={(text) => {this.setState({email: text})}} label="Email" value={this.state.email}/>
            <Text style={styles.header}>CHANGE PASSWORD</Text>
            <FormInput onChangeText={(text) => {this.setState({password: text})}} label="New Password" password={true} value={this.state.password}/>
            <FormInput onChangeText={(text) => {this.setState({repeatPassword: text})}} label="Repeat New Password" password={true} value={this.state.repeatPassword}/>
          </View>
        </ScrollView>

        <ButtonBottom text="Save Changes" image={require('../images/billing_info/btn-blue.png')} onPress={() => this.props.navigation.goBack()}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  wrapper: {
    paddingLeft:wp(6.66667),
    paddingRight:wp(6.66667),
  },
  header: {
    marginTop: 40,
    marginBottom: 10,
    color: '#ccc',
    fontFamily: 'nunito-regular',
    fontSize: 16,
  }
})

export default Screen
