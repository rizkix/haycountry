import React, { Component } from 'react';
import HeaderBar from '../components/HeaderBar';
import FormInput from '../components/FormInput';
import ButtonBottom from '../components/ButtonBottom';
import { TextInputMask } from 'react-native-masked-text'
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View, 
  ScrollView, 
  Button,
  Dimensions,
} from 'react-native'

import { wp, width, height }  from '../utils'



import { headerStyle } from '../styles';
class Screen extends Component {
  static navigationOptions = {
    headerTitle: <View style={{justifyContent:'center', alignItems:'center', alignSelf: 'center', flexGrow: 1}}>
      <HeaderBar title='Billing Info'/>
      </View>,
    headerStyle: headerStyle,
    headerRight: <View/>
  }

  constructor(props) {
    super(props)
    this.state = {
      nameOnCard: 'Modestas Baranauskas',
      cardNumber: '1523-2156-6546-5486',
      expireMonth: 'December',
      expireDay: '15',
      cvv: '123',

      fullName: 'Modestas Baranauskas',
      address: 'Zukausko 18-55',
      city: 'Vilnius',
      zip: '08253'
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.wrapper}>
          <View style={{marginBottom:50}}>
            <Text style={styles.header}>CREDIT CARD INFO</Text>
            <FormInput onChangeText={(text) => {this.setState({nameOnCard: text})}} label="Name on Card" value={this.state.nameOnCard}/>
            <View style={{width: wp(86.66666)}}>
              <Text style={styles.label}>Card Number</Text>
              <TextInputMask 
                ref='ccInput'
                underlineColorAndroid='rgba(255,255,255,0)'
                type={'credit-card'}
                value={this.state.cardNumber}
                style={[styles.input]}
              />
              <Image style={styles.visa} source={require('../images/billing_info/visa.png')}/>
            </View>
            <FormInput onChangeText={(text) => {this.setState({expireMonth: text})}} label="Expiration Month" value={this.state.expireMonth}/>
            <FormInput onChangeText={(text) => {this.setState({expireDay: text})}} label="Expiration Day" value={this.state.expireDay}/>
            <FormInput onChangeText={(text) => {this.setState({cvv: text})}} label="CVV Number" value={this.state.cvv}>
              <Text style={styles.digits}>3 digits on back of card</Text>
            </FormInput>

            <Text style={styles.header}>BILLING ADDRESS</Text>
            <FormInput onChangeText={(text) => {this.setState({fullName: text})}} label="Full Name"  value={this.state.fullName}/>
            <FormInput onChangeText={(text) => {this.setState({city: text})}} label="City"  value={this.state.city}/>
            <FormInput onChangeText={(text) => {this.setState({address: text})}} label="Address"  value={this.state.address}/>
            <FormInput onChangeText={(text) => {this.setState({zip: text})}} label="ZIP Code"  value={this.state.zip}/>
          </View>
        </ScrollView>

        <ButtonBottom text="Save Changes" image={require('../images/billing_info/btn-blue.png')} onPress={() => this.props.navigation.goBack()}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  wrapper: {
    paddingLeft:wp(6.66667),
    paddingRight:wp(6.66667),
  },
  header: {
    marginTop: 40,
    marginBottom: 10,
    color: '#ccc',
    fontFamily: 'nunito-regular',
    fontSize: 16,
  },
  label: {
    fontFamily: 'nunito-bold',
    fontSize:14,
  },
  input: {
    marginTop: 0,
    paddingTop: 0,
    marginLeft: 0,
    paddingLeft: 0,
    paddingBottom: 14,
    borderBottomColor: '#d3d3d3',
    borderBottomWidth: 1,
    textAlign: 'left',
    fontSize: 19,
    color:'#000',
    fontFamily: 'nunito-regular',
  },
  visa: {
    position: 'absolute',
    top: 33,
    right: 0,
    width: 32,
    height: 20,
  },
  digits:{
    position: 'absolute',
    top: 35,
    right: 0,
    fontFamily: 'nunito-regular',
    fontSize: 12,
    color:'#53a235'
  }
})

export default Screen
