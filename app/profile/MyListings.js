import React, { Component } from 'react';
import HeaderBar from '../components/HeaderBar';
import FormInput from '../components/FormInput';
import ButtonBottom from '../components/ButtonBottom';

import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';

import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View, 
  ScrollView, 
  Button,
} from 'react-native'

import { wp, width, height }  from '../utils'
import { NavigationActions } from 'react-navigation';

import { headerStyle } from '../styles';

class Menu extends Component {
  render() {
    const lists = {
      edit: {
        text: 'Edit Listing',
        icon: require("../images/profile/icon-edit.png"),
        screen: 'EDIT_LISTING'
      },
      preview: {
        text: 'Preview Listing',
        icon: require("../images/my_listings/icon-view.png"),
        screen: 'PREVIEW_LISTING'
      },
      renew: {
        text: 'Renew Listing',
        icon: require("../images/my_listings/icon-reload.png"),
        screen: 'RENEW_LISTING'
      },
      delete: {
        text: 'Delete',
        icon: require("../images/my_listings/icon-garbage.png"),
        screen: 'DELETE_LISTING'
      }
    }
    const menuImage = lists[this.props.obj].icon
    const menuText = lists[this.props.obj].text
    const menuScreen = lists[this.props.obj].screen

    return (
      <TouchableOpacity activeOpacity={0.75} style={this.props.style ? this.props.style : styles.buttonMenu} onPress={this.props.onPress}>
        <Image style={styles.menuImage} source={menuImage}/>
        <Text style={[styles.menuText, this.props.textStyle]}>{menuText}</Text>
      </TouchableOpacity>
    )
  }
}

const GoToListing = NavigationActions.navigate({
  routeName: 'HOME',
  params: {},
  action: NavigationActions.navigate({ routeName: 'LISTING' }),
});

class MyListing extends Component {
  render(){
    const openValue = wp(46.66667) + 25
  	return (
      <View style={styles.listingWrap}>
        <SwipeRow leftOpenValue={openValue} disableLeftSwipe={true} preview={true}>
        	<View style={styles.listingLeft}>
        		<View style={{marginTop:5, marginBottom:5}}>
              <Menu onPress={() => this.props.editListing()} obj="edit"/>
              <Menu onPress={() => this.props.goToListing()} obj="preview"/>
              <Menu onPress={() => this.props.renewListing()} obj="renew"/>
              <Menu onPress={() => this.props.removeListing()} style={styles.buttonMenuBottom} textStyle={{color:'#b0b0b0'}} obj="delete"/>
            </View>
        	</View>
        	<View style={styles.listingRight}>
        		<View style={styles.listingItem}>
        			<Image resizeMode="cover" style={styles.listingImage} source={this.props.image}/>
        			<View style={styles.listingBody}>
        				<Text style={styles.listingTitle}>{this.props.title}</Text>
        			</View>
        		</View>
        	</View>
        </SwipeRow>
      </View>
    )
  }
}
class Screen extends Component {
  static navigationOptions = {
    headerTitle: <View style={{justifyContent:'center', alignItems:'center',alignSelf: 'center', flexGrow: 1}}>
      <HeaderBar title='My Listings'/>
      </View>,
    headerStyle: headerStyle,
    headerRight: <View/>
  }

  constructor(props) {
    super(props)
    this.state = {
      myListings: [{
        image: require('../images/listings/img-06.jpg'),
        title: 'Alrounder, Hunter, Weight Carrier, Sport'
      },{
        image: require('../images/listings/img-07.jpg'),
        title: 'Alrounder, Hunter, Weight Carrier, Sport'
      },{
        image: require('../images/listings/img-08.jpg'),
        title: 'Alrounder, Hunter, Weight Carrier, Sport'
      }]
    }

    this.goToListing = this.goToListing.bind(this)
    this.editListing = this.editListing.bind(this)
    this.renewListing = this.renewListing.bind(this)
    this.removeListing = this.removeListing.bind(this)
  }

  goToListing() {
    this.props.navigation.dispatch(GoToListing)
  }

  editListing() {
    alert('edit listing')
  }

  renewListing() {
    alert('renew listing')
  }

  removeListing() {
    alert('remove listing')
  }



  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.wrapper}>
          <Text style={styles.header}>YOU HAVE 3 ACTIVE LISTINGS</Text>
          <Text style={styles.lead}>Edit, renew or delete your old listings</Text>
          <View style={{marginBottom:20}}>
          {
              this.state.myListings.map((u, i) => {
                return <MyListing key={i} image={u.image} title={u.title} 
                  goToListing={this.goToListing}
                  editListing={this.editListing}
                  removeListing={this.removeListing}
                  renewListing={this.renewListing}
                >
                  </MyListing>
              })
          }
          </View>
        </ScrollView>

        <ButtonBottom text="Submit New Listing" image={require('../images/contact_seller/btn-green.png')} onPress={() => this.props.navigation.navigate('SUBMIT')}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  wrapper: {
  },
  header: {
    marginTop: 30,
    marginBottom: 5,
    color: '#000',
    fontFamily: 'nunito-regular',
    fontSize: 16,
    paddingLeft:wp(6.66667),
    paddingRight:wp(6.66667),
  },
  lead: {
    marginBottom: 25,
    color: '#5e5e5e',
    fontFamily: 'nunito-regular',
    fontSize: 14,
    lineHeight: 20,
    paddingLeft:wp(6.66667),
    paddingRight:wp(6.66667),
  },
  listingWrap:{
  	flex: 1,
  	flexDirection: 'row',
  	marginBottom:10,
  },
  listingLeft:{
    paddingLeft:wp(6.66667),
  },
  listingRight:{
  },
  buttonMenu: {
    borderBottomColor: '#d3d3d3',
    borderBottomWidth: 1,
    height: wp(19),
    alignItems: 'center',
    flexDirection: 'row',
    width: wp(46.66667),
  },
  buttonMenuBottom: {
    height: wp(19),
    alignItems: 'center',
    flexDirection: 'row',
    width: wp(46.66667),
  },
  menuImage: {
    width: 28,
    height: 28
  },
  menuText: {
    color: '#000',
    fontFamily: 'nunito-bold',
    fontSize: 17,
    marginLeft:12
  },
  listingItem:{
  	overflow:'hidden',
  	width:wp(85.86667),
    marginTop:15,
    marginBottom:25,
    marginLeft: wp(6.66667),
    marginRight: wp(6.66667),
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 8,
    borderRadius: 8,
    elevation: 8,
    backgroundColor:'#fff',
  },
  listingImage:{
  	height:wp(52),
  	width:wp(85.86667),
  },
  listingBody:{
  	paddingTop:20,
  	paddingBottom:25,
  	paddingLeft: 25,
  	paddingRight: 25,
  	backgroundColor:'#fff'
  },
  listingTitle:{
  	fontSize: 18,
  	lineHeight: 28,
  	color: '#000',
  	fontFamily: 'nunito-bold'
  }
})

export default Screen
