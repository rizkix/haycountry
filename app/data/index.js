export const horses = [
  {
    title: 'Alrounder, Hunter, Weight, Carrier, Sport',
    category: {
      id: 1,
      name: 'Horses',
      ref: ''
    },
    image: require('../images/home/img-01.jpg'),
    price: '$595',
    safe_address: 'South East, Staplehurst',
    distance_from: 56000,
    liked: false,
    latitude: 37.78825,
    longitude: -122.4324,
  },
  {
    title: 'Allrounder/ Event Irish Mare Dorset',
    category: {
      id: 1,
      name: 'Horses',
      ref: ''
    },
    image: require('../images/home/img-02.jpg'),
    price: '$595',
    safe_address: 'South East, Staplehurst',
    distance_from: 56000,
    liked: false,
    latitude: 37.78825,
    longitude: -122.4458,
  },
  {
    title: 'Alrounder, Hunter, Weight, Carrier, Sport',
    category: {
      id: 1,
      name: 'Horses',
      ref: ''
    },
    image: require('../images/home/img-01.jpg'),
    price: '$595',
    safe_address: 'South East, Staplehurst',
    distance_from: 56000,
    liked: false,
    latitude: 37.78525,
    longitude: -122.4839,
  }
]

export const hays = [
  {
    title: 'Coastal Mixed Large Round',
    category: {
      id: 1,
      name: 'Hay',
      ref: ''
    },
    image: require('../images/home/img-03.jpg'),
    price: '$5',
    price_unit: '/ bale',
    safe_address: 'South East, Staplehurst',
    distance_from: 56000,
    liked: false
  },
  {
    title: 'Timothy Mix Small Square',
    category: {
      id: 1,
      name: 'Hay',
      ref: ''
    },
    image: require('../images/home/img-04.jpg'),
    price: '$6',
    price_unit: '/ bale',
    safe_address: 'South East, Staplehurst',
    distance_from: 56000,
    liked: false
  },
  {
    title: 'Coastal Mixed Large Round',
    category: {
      id: 1,
      name: 'Hay',
      ref: ''
    },
    image: require('../images/home/img-03.jpg'),
    price: '$5',
    price_unit: '/ bale',
    safe_address: 'South East, Staplehurst',
    distance_from: 56000,
    liked: false
  }
]

export const recommended = [
  {
    title: 'American Club Calf Bull South East Texas',
    category: {
      id: 1,
      name: 'Cattles',
      ref: ''
    },
    image: require('../images/home/img-05.jpg'),
    price: '$525',
    price_unit: '/ cattle',
    safe_address: 'South East, Staplehurst',
    distance_from: 56000,
    liked: false
  },
  {
    title: 'Timothy Mix Small Square',
    category: {
      id: 1,
      name: 'Cattles',
      ref: ''
    },
    image: require('../images/home/img-04.jpg'),
    price: '$6',
    price_unit: '/ bale',
    safe_address: 'South East, Staplehurst',
    distance_from: 56000,
    liked: false
  },
  {
    title: 'Alrounder, Hunter, Weight, Carrier, Sport',
    category: {
      id: 1,
      name: 'Cattles',
      ref: ''
    },
    image: require('../images/home/img-01.jpg'),
    price: '$595',
    safe_address: 'South East, Staplehurst',
    distance_from: 56000,
    liked: false
  }
]
