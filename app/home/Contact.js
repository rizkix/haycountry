import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View, 
  ScrollView, 
  Button,
  Dimensions,
} from 'react-native'

import { wp, width, height }  from '../utils'
import Modal from 'react-native-modal';
import FormInput from '../components/FormInput';
import Slider from 'react-native-slider'

import ButtonBottom from '../components/ButtonBottom';
class Screen extends Component {

  constructor(props) {
    super(props)
    this.state = {
      name: '',
      email: '',
      phone: '',
      message: '',
    }
  }

  render() {
    return (
        <Modal isVisible={this.props.isModalVisible} style={styles.modalContent} useNativeDriver={true} onBackButtonPress={this.props.closeModal} onModalShow={this.props.onModalShow} onModalHide={this.props.onModalShow}>
          <TouchableOpacity activeOpacity={0.75} style={styles.buttonClose} onPress={this.props.closeModal}>
            <Image style={styles.closeImage} source={require("../images/contact_seller/icon-close.png")}/>
          </TouchableOpacity>

          <ScrollView style={styles.scrollview}>
            <View style={styles.top}>
              <Text style={styles.header}>Contact Seller</Text>
              <View style={styles.info}>
                <Image style={styles.infoImage} source={require('../images/contact_seller/icon-user.png')} />
                <Text style={styles.infoText}>Robert Zinnerman</Text>
              </View>
              <View style={styles.info}>
                <Image style={styles.infoImage} source={require('../images/contact_seller/icon-phone.png')} />
                <Text style={styles.infoText}>+44 645 64123</Text>
              </View>
            </View>
              <View style={styles.inputWrap}>
              <FormInput onChangeText={(text)=>{this.setState({name: text})}}  placeholder="Enter your name" value={this.state.name}/>
              <FormInput onChangeText={(text)=>{this.setState({email: text})}}  placeholder="Enter your email" value={this.state.email} />
              <FormInput onChangeText={(text)=>{this.setState({phone: text})}}  placeholder="Enter phone" value={this.state.phone}/>
              <FormInput onChangeText={(text)=>{this.setState({message: text})}}  placeholder="Message" value={this.state.message}/>

            </View>
          </ScrollView>

          <ButtonBottom text={"Send Your Message"} image={require('../images/contact_seller/btn-green.png')} onPress={this.props.closeModal}/>

        </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  inputWrap: {
    paddingLeft: wp(6.6667),
    paddingRight: wp(6.6667),
    marginTop: 40,
    width: wp(100)
  },
  top: {
    marginTop: 100,
    paddingHorizontal: wp(6.6667),
  },
  header: {
    color: '#000',
    fontSize: 25,
    marginBottom: 10,
    fontFamily: 'nunito-bold'
  },
  info: {
    flexDirection: 'row',
    marginTop: 10
  },
  infoImage: {
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 8,
    width: 16,
    height: 16
  },
  infoText: {
    color: '#878787',
    fontSize: 16,
    fontFamily: 'nunito-regular'
  },
  modalContent:{
    height: height,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: undefined,
    margin: 0,
  },
  buttonClose:{
    position:'absolute',
    right:25,
    top:25,
    zIndex:10,
  },
  closeImage:{
    width:30,
    height:30
  },
  gpsImage: {
    position: 'absolute',
    top: 17,
    right: 0,
    width: 25,
    height: 25,
  },
  distance: {
    marginTop: 50,
    color: '#8d8d8d',
    fontSize: 14,
    fontFamily: 'nunito-bold'
  },
  blueText: {
    color: '#1264ca'
  },
  sliderContainer:{
    paddingLeft: wp(7.6),
    paddingRight: wp(7),
    marginTop:10,
  },
  sliderBg:{
    position:'absolute',
    top:wp(0.7),
    left: wp(4.2),
    width:wp(91.6),
    height:wp(9.6),
  },
  sliderTrack:{
    position:'absolute',
    left:0,
    right:0,
    top:10,
    height:10,
    backgroundColor:'#fff',
    elevation:3,
    borderRadius:10,
    zIndex:1,
  },
  slider:{
  },
  thumbStyle:{
    elevation:2,
  },
  viewBottom: {
    width: "100%",
    zIndex:40,
    elevation: 19,
    backgroundColor: '#fff',
    paddingLeft: wp(6.6667),
    paddingRight: wp(6.6667),
    bottom:0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop:20,
    paddingBottom:20,
  },
  searchWrap: {
    width: wp(33),
    borderRadius: 4,
    borderColor: '#cbcbcb',
    borderWidth: 1,
    height: wp(11.7333),
    alignItems: 'center',
    justifyContent:'center',
  },
  buttonSave: {
    alignItems: 'center',
    justifyContent:'center',
  },
  saveText:{
    fontFamily: 'nunito-regular',
    color: '#828282',
    fontSize: 15,
  },
  buttonSearch: {
    justifyContent:'center',
    alignItems: 'center',
  },
  searchImage: {
    width: wp(46.26667),
    height: wp(11.73334)
  },
  searchText: {
    position: 'absolute',
    fontFamily: 'nunito-regular',
    color: '#fff',
    fontSize: 15,
    marginLeft: 10,
    zIndex: 10
  },

})


export default Screen;
