import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  ImageBackground,
  View, 
  ScrollView, 
  Button,
  Dimensions,
} from 'react-native'
import { wp } from '../utils'
import MapView, { Marker,Callout } from 'react-native-maps';

import HeaderBar from '../components/HeaderBar';
import SearchBar from '../components/SearchBar';
import SearchBarRight from '../components/SearchBarRight';
import SearchModal from './Search';
import { NavigationActions } from 'react-navigation';



import { headerStyle } from '../styles';
import { horses } from '../data'

const GoToListing = NavigationActions.navigate({
  routeName: 'HOME',
  params: {},
  action: NavigationActions.navigate({ routeName: 'LISTING' }),
});

class Screen extends Component {
  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state;
    return {
      headerLeft: <SearchBar count='2.153'/>,
      headerStyle: headerStyle,
      headerRight: <SearchBarRight onPress={() => params.toggleModal()} image={require('../images/listings_map/icon-filters.png')} />,
      headerMode: 'float'
    }
  }

  constructor(props) {
    super(props)
    this.state = { data: horses, isModalVisible: false, activeItem: null }
    this.toggleModal = this.toggleModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.toggleLike = this.toggleLike.bind(this)
    this.changeActiveItem = this.changeActiveItem.bind(this)
    this.goToListing = this.goToListing.bind(this)
  }

  componentDidMount() {
    this.props.navigation.setParams({
      toggleModal: this.toggleModal
    });
  }

  toggleModal() {
    this.setState({ isModalVisible: !this.state.isModalVisible})
  }

  toggleLike(key) {
    let items = this.state.data
    items[key].liked = !items[key].liked
    this.setState({data: items})
  }

  closeModal() {
    this.setState({ isModalVisible: false})
  }

  changeActiveItem(key) {
    this.setState({ activeItem: key })
  }
  
  goToListing() {
    this.props.navigation.dispatch(GoToListing)
  }

  render() {
    const marker = {
    }

    return (
      <View style={styles.mapContainer}>
        <TouchableOpacity activeOpacity={0.75} style={styles.buttonSeeListings} onPress={() => this.props.navigation.goBack()}>
          <Image style={styles.seeListingsImage} source={require("../images/listings_map/icon-back_active.png")}/>
          <Text style={styles.seeListingsText}>Back to list view</Text>
        </TouchableOpacity>

        <MapView
          style={styles.map}
          moveOnMarkerPress={false}
          initialRegion={{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}>
          {
            this.state.data.map((d, i) => {
              const markerImage = this.state.activeItem === i ? require('../images/listings_map/icon-map-marker_active.png') : require('../images/listings_map/icon-map-marker.png')

              return (
                <Marker
                  coordinate={d}
                  key={i}
                  image={markerImage}
                  onPress={() => {this.changeActiveItem(i)}}
                >
                  <Callout tooltip={true} onPress={() => { this.goToListing() }}>
                    <View style={styles.mapPopup}>
                      <Image style={styles.image} source={d.image} />
                      <View style={styles.priceBox}>
                        <Text style={styles.price}>{d.price}</Text>
                      </View>
                      <Image style={styles.caret} source={require('../images/listings_map/caret.png')} />
                    </View>
                  </Callout>
                </Marker>
              )
            })
          }
        </MapView>

      <SearchModal isModalVisible={this.state.isModalVisible} closeModal={this.closeModal} />
      </View>
    )
  }
}

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const styles = StyleSheet.create({
  mapContainer: {
    height: width,
    width: height,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    flex: 1,
    overflow: 'hidden',
    backgroundColor: '#fff',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    overflow: 'hidden'
  },
  marker: {
    width: 50,
    height: 50,
  },
  buttonSeeListings: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 30,
    left: wp(50),
    marginLeft:-120,
    zIndex: 20,
    width: 240,
    backgroundColor: '#fff',
    elevation: 4,
    flexDirection: 'row',
    paddingLeft: 6,
    paddingRight: 6,
    paddingTop: 17,
    paddingBottom: 17,
    borderRadius: 6,

    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 3,
  },
  seeListingsText: {
    fontFamily: 'nunito-regular',
    fontSize: 20,
    marginLeft: 15,
    color: '#000'
  },
  seeListingsImage: {
    width: 22,
    height: 22
  },
  mapPopup:{
    //flexDirection: 'column',
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:35,
  },
  image: {
    //position: 'absolute',
    width: wp(32.66667),
    height: wp(19.33334),
    borderRadius:8,
    overflow: 'hidden'
  },
  priceBox: {
    position: 'absolute',
    top:wp(16),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    elevation: 4,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 5,
    paddingBottom: 5,
    borderRadius: 15,
    margin:5,
  },
  caret:{
    width:20,
    height:10,
    position: 'absolute',
    bottom:-34,
    left:wp(14),
    resizeMode:'cover'
  },
  price: {
    color: '#429e32',
    fontFamily: 'nunito-bold',
    fontSize: 17,
  }
})




export default Screen;
