import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View, 
  ScrollView, 
  Button,
  Dimensions,
} from 'react-native'

import { wp, width, height }  from '../utils'
import Modal from 'react-native-modal';
import FormInput from '../components/FormInput';
import Slider from 'react-native-slider'


class Screen extends Component {

  constructor(props) {
    super(props)
    this.state = {
      keywords: '',
      category: 'Horses',
      location: '',
      distance: 50,
    }
  }

  render() {
    return (
        <Modal isVisible={this.props.isModalVisible} style={styles.modalContent} useNativeDriver={true} onBackButtonPress={this.props.closeModal} onModalShow={this.props.onModalShow} onModalHide={this.props.onModalShow}>
          <TouchableOpacity activeOpacity={0.75} style={styles.buttonClose} onPress={this.props.closeModal}>
            <Image style={styles.closeImage} source={require("../images/contact_seller/icon-close.png")}/>
          </TouchableOpacity>

          <ScrollView style={styles.scrollview}>
            <View style={styles.inputWrap}>
              <FormInput onChangeText={(text)=>{this.setState({keywords: text})}}  placeholder="Enter your keywords" value={this.state.keywords}/>
              <FormInput onChangeText={(text)=>{this.setState({category: text})}}  label="Choose category" value={this.state.category} checked={true}/>
              <FormInput onChangeText={(text)=>{this.setState({location: text})}}  placeholder="Enter location" value={this.state.location}>
                <Image style={styles.gpsImage} source={require('../images/search/icon-gps.png')}/>
              </FormInput>

              <Text style={styles.distance}>
                <Text>Distance from you</Text>
                <Text style={styles.blueText}> ({this.state.distance} miles)</Text>
              </Text>
            </View>
            <View style={styles.sliderContainer}>
              <Image style={styles.sliderBg} source={require('../images/search/bg-slider.png')} />
              <Slider style={styles.slider} step={1} 
                trackStyle={{backgroundColor: "#fff", height:wp(1.6), borderRadius:wp(1.6)}}
                onValueChange={(value) => this.setState({distance: value})}
                minimumTrackTintColor="#1264ca"
                thumbTintColor="#1264ca"
                minimumValue={0} maximumValue={100} value={this.state.distance} />
            </View>
          </ScrollView>

            <View style={styles.viewBottom}>
              <TouchableOpacity activeOpacity={0.75} style={styles.buttonSave} onPress={this.props.closeModal}>
                <View style={styles.searchWrap}>
                  <Text style={styles.saveText}>Save search</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity activeOpacity={0.75} style={styles.buttonSearch} onPress={this.props.closeModal}>
                <Image style={styles.searchImage} source={require("../images/search/btn-green-round-sm-04.png")}/>
                <Text style={styles.searchText}>Search listings</Text>
              </TouchableOpacity>
            </View>

        </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  inputWrap: {
    paddingLeft: wp(6.6667),
    paddingRight: wp(6.6667),
    marginTop: 70,
    width: wp(100)
  },
  modalContent:{
    height: height,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: undefined,
    margin: 0,
  },
  buttonClose:{
    position:'absolute',
    right:25,
    top:25,
    zIndex:10,
  },
  closeImage:{
    width:30,
    height:30
  },
  gpsImage: {
    position: 'absolute',
    top: 17,
    right: 0,
    width: 25,
    height: 25,
  },
  distance: {
    marginTop: 50,
    color: '#8d8d8d',
    fontSize: 14,
    fontFamily: 'nunito-bold'
  },
  blueText: {
    color: '#1264ca'
  },
  sliderContainer:{
    paddingLeft: wp(7.6),
    paddingRight: wp(7),
    marginTop:10,
  },
  sliderBg:{
    position:'absolute',
    top:wp(0.7),
    left: wp(4.2),
    width:wp(91.6),
    height:wp(9.6),
  },
  sliderTrack:{
    position:'absolute',
    left:0,
    right:0,
    top:10,
    height:10,
    backgroundColor:'#fff',
    elevation:3,
    borderRadius:10,
    zIndex:1,
  },
  slider:{
  },
  thumbStyle:{
    elevation:2,
  },
  viewBottom: {
    width: "100%",
    zIndex:40,
    elevation: 19,
    backgroundColor: '#fff',
    paddingLeft: wp(6.6667),
    paddingRight: wp(6.6667),
    bottom:0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop:15,
    paddingBottom:15,

    shadowColor: '#000',
    shadowOpacity: 0.15,
    shadowOffset: { width: 0, height: -2 },
    shadowRadius: 2,
  },
  searchWrap: {
    width: wp(33),
    borderRadius: 4,
    borderColor: '#cbcbcb',
    borderWidth: 1,
    height: wp(11.7333),
    alignItems: 'center',
    justifyContent:'center',
  },
  buttonSave: {
    alignItems: 'center',
    justifyContent:'center',
  },
  saveText:{
    fontFamily: 'nunito-regular',
    color: '#828282',
    fontSize: 15,
  },
  buttonSearch: {
    justifyContent:'center',
    alignItems: 'center',
  },
  searchImage: {
    width: wp(46.26667),
    height: wp(11.73334)
  },
  searchText: {
    position: 'absolute',
    fontFamily: 'nunito-regular',
    color: '#fff',
    fontSize: 15,
    marginLeft: 10,
    zIndex: 10
  }
})


export default Screen;
