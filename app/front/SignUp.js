import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  TextInput,
  Image,
  Button,
  View,
  Dimensions,
} from 'react-native'
import { wp, width, height }  from '../utils'

class Screen extends Component {

 constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      username: '',
      password: '' ,
      borderColorFirstName: '#d3d3d3',
      borderColorLastName: '#d3d3d3',
      borderColorEmail: '#d3d3d3',
      borderColorPassword: '#d3d3d3'
    };
  }


  onFocusFirstName() {
    this.setState({
      borderColorFirstName: '#6fc842'
    })
  }
  onBlurFirstName() {
    this.setState({
      borderColorFirstName: '#d3d3d3'
    })
  }
  onFocusLastName() {
    this.setState({
      borderColorLastName: '#6fc842'
    })
  }
  onBlurLastName() {
    this.setState({
      borderColorLastName: '#d3d3d3'
    })
  }
  onFocusEmail() {
    this.setState({
      borderColorEmail: '#6fc842'
    })
  }
  onBlurEmail() {
    this.setState({
      borderColorEmail: '#d3d3d3'
    })
  }
  onFocusPassword() {
    this.setState({
      borderColorPassword: '#6fc842'
    })
  }
  onBlurPassword() {
    this.setState({
      borderColorPassword: '#d3d3d3'
    })
  }

  render() {
    return (
      <View style={styles.container}>

        <Image style={styles.background} source={require("../images/signin/bg.jpg")}/>
        <Image style={styles.logo} source={require("../images/logo.png")}/>
        <Text style={styles.signinText}>Sign Up</Text>

        <TextInput
          underlineColorAndroid='rgba(255,255,255,0)'
          placeholder='Your fistname'
          placeholderTextColor="#000"
          style={[styles.input, {borderBottomColor: this.state.borderColorFirstName}]}
          onChangeText={(firstName) => this.setState({firstName})}
          value={this.state.firstName}
          onBlur={ () => this.onBlurFirstName() }
          onFocus={ () => this.onFocusFirstName() }
        />

        <TextInput
          underlineColorAndroid='rgba(255,255,255,0)'
          placeholder='Your lastname'
          placeholderTextColor="#000"
          style={[styles.input, {borderBottomColor: this.state.borderColorLastName}]}
          onChangeText={(lastName) => this.setState({lastName})}
          value={this.state.lastName}
          onBlur={ () => this.onBlurLastName() }
          onFocus={ () => this.onFocusLastName() }
        />

        <TextInput
          underlineColorAndroid='rgba(255,255,255,0)'
          placeholder='Enter your email'
          placeholderTextColor='#000'
          keyboardType= 'email-address'
          style={[styles.input, {borderBottomColor: this.state.borderColorEmail}]}
          onChangeText={(username) => this.setState({username})}
          value={this.state.username}
          onBlur={ () => this.onBlurEmail() }
          onFocus={ () => this.onFocusEmail() }
        />

        <TextInput
          underlineColorAndroid='rgba(255,255,255,0)'
          placeholder='Enter password'
          placeholderTextColor="#000"
          style={[styles.input, {borderBottomColor: this.state.borderColorPassword}]}
          secureTextEntry={true}
          onChangeText={(password) => this.setState({password})}
          value={this.state.password}
          onBlur={ () => this.onBlurPassword() }
          onFocus={ () => this.onFocusPassword() }
        />
        
        <TouchableOpacity activeOpacity={0.75} style={styles.button} onPress={() => this.props.navigation.replace('Dashboard')}>
          <Image style={styles.buttonImage} source={require("../images/signin/btn-green.png")}/>
          <Image style={styles.arrow} source={require("../images/signin/icon-arrow-right.png")}/>
        </TouchableOpacity>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    width: 150,
    height: 26
  },
  signinText: {
    marginTop: 10,
    marginBottom: 30,
    fontSize: 20,
    color: '#9b9b9b',
    fontFamily: 'nunito-light'
  },
  input: {
    paddingBottom: 4,
    width: '76%',
    borderBottomColor: '#d3d3d3',
    borderBottomWidth: 1,
    textAlign: 'center',
    fontSize: 18,
    marginTop: 20,
    color:'#000',
    fontFamily: 'nunito-bold',
  },
  button: {
    marginTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonImage: {
    width: wp(28.5),
    height: wp(28.5),
    zIndex: -1
  },
  arrow: {
    top: wp(9.6),
    position:'absolute',
    width: wp(5.33334),
    height: wp(5.33334),
    zIndex: 10
  },
  background: {
    position: 'absolute',
    top: 0,
    zIndex: 0,
    width: width,
    height: height,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
})

export default Screen;
