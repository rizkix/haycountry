import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  TextInput,
  Image,
  Button,
  View,
  Dimensions,
} from 'react-native'

class Screen extends Component {

 constructor(props) {
    super(props);
    this.state = { 
      username: '', 
      password: '', 
      borderColorEmail: '#d3d3d3', 
      borderColorPassword: '#d3d3d3' 
    };
  }

  onFocusEmail() {
    this.setState({
      borderColorEmail: '#6fc842'
    })
  }
  onBlurEmail() {
    this.setState({
      borderColorEmail: '#d3d3d3'
    })
  }
  onFocusPassword() {
    this.setState({
      borderColorPassword: '#6fc842'
    })
  }
  onBlurPassword() {
    this.setState({
      borderColorPassword: '#d3d3d3'
    })
  }

  render() {
    return (
      <View style={styles.container}>

        <Image resizeMode="cover" style={styles.background} source={require("../images/signin/bg.jpg")}/>
        <Image style={styles.logo} source={require("../images/logo.png")}/>
        <Text style={styles.signinText}>Sign In</Text>

        <TextInput
          underlineColorAndroid='rgba(255,255,255,0)'
          placeholder='Enter your email'
          placeholderTextColor='#000'
          keyboardType= 'email-address'
          style={[styles.input, {borderBottomColor: this.state.borderColorEmail}]}
          onChangeText={(username) => this.setState({username})}
          value={this.state.username}
          onBlur={ () => this.onBlurEmail() }
          onFocus={ () => this.onFocusEmail() }
        />

        <TextInput
          underlineColorAndroid='rgba(255,255,255,0)'
          placeholder='*****'
          placeholderTextColor="#000"
          style={[styles.input, {borderBottomColor: this.state.borderColorPassword}]}
          secureTextEntry={true}
          onChangeText={(password) => this.setState({password})}
          value={this.state.password}
          onBlur={ () => this.onBlurPassword() }
          onFocus={ () => this.onFocusPassword() }
        />

        <TouchableOpacity activeOpacity={0.75} style={styles.button} onPress={() => this.props.navigation.replace('Dashboard')}>
          <Image style={styles.buttonImage} source={require("../images/signin/btn-green.png")}/>
          <Image style={styles.arrow} source={require("../images/signin/icon-arrow-right.png")}/>
        </TouchableOpacity>

        <TouchableOpacity activeOpacity={0.75} style={styles.button2} onPress={() => this.props.navigation.navigate('Dashboard')}>
          <Image style={styles.buttonFB} source={require("../images/signin/btn-facebook.png")}/>
          <View style={styles.buttonFBText}>
            <Image style={styles.iconFB} source={require("../images/signin/icon-facebook.png")}/>
            <Text style={styles.textFB}>Login Through Facebook</Text>
          </View>
        </TouchableOpacity>
        
        <View style={{flexDirection: 'row'}}>
          <Text style={{fontSize: 16, color:'#6e6e6e', fontFamily: 'nunito-light'}}>Don't have an account? </Text>
          <TouchableOpacity activeOpacity={1} onPress={()=> this.props.navigation.navigate('SignUp')}>
            <Text style={{fontSize: 16, color: '#58a335', fontFamily: 'nunito-bold'}}>Sign Up</Text>
          </TouchableOpacity>
        </View>

      </View>
    );
  }
}

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
function wp (percentage) {
  const value = (percentage * width) / 100;
  return Math.round(value);
}
const styles = StyleSheet.create({
  logo: {
    width: 150,
    height: 26
  },
  signinText: {
    marginTop: 10,
    marginBottom: 30,
    fontSize: 20,
    color: '#9b9b9b',
    fontFamily: 'nunito-light'
  },
  input: {
    paddingBottom: 4,
    width: '76%',
    borderBottomColor: '#d3d3d3',
    borderBottomWidth: 1,
    textAlign: 'center',
    fontSize: 18,
    marginTop: 20,
    color:'#000',
    fontFamily: 'nunito-bold',
  },
  button: {
    marginTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonImage: {
    width: wp(28.5),
    height: wp(28.5),
    zIndex: -1
  },
  arrow: {
    top: wp(9.6),
    position:'absolute',
    width: wp(5.33334),
    height: wp(5.33334),
    zIndex: 10
  },
  button2: {
    marginTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonFB: {
    width: wp(90.8),
    height: wp(21.86667),
    zIndex: -1
  },
  buttonFBText: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    position: 'absolute',
    width: wp(83),
    top: 6,
    height: wp(14),
    zIndex: -1,
  },
  iconFB: {
    width: 15,
    height: 15,
    zIndex: 10
  },
  textFB: {
    fontFamily: 'nunito-regular',
    color: '#fff',
    fontSize: 15,
    marginLeft: 10,
    zIndex: 10
  },
  background: {
    position: 'absolute',
    top: 0,
    zIndex: 0,
    width: width,
    height: height,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
})

export default Screen;
