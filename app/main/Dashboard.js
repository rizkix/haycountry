import { TabNavigator, TabBarBottom } from 'react-navigation';
import React, { Component } from 'react';
import { Text, Image } from 'react-native';

import Home from './Home';
import Explore from './Explore';
import Submit from './Submit';
import Saved from './Saved';
import Profile from './Profile';

class TabIcon extends Component {
  render() {
    const icon = {
      home: require("../images/home/icon-nav-home.png"),
      homeActive: require("../images/home/icon-nav-home_active.png"),
      explore: require("../images/home/icon-nav-explore.png"),
      exploreActive: require("../images/home/icon-nav-explore_active.png"),
      profile: require("../images/home/icon-nav-profile.png"),
      profileActive: require("../images/home/icon-nav-profile_active.png"),
      submit: require("../images/home/icon-nav-submit.png"),
      submitActive: require("../images/home/icon-nav-submit_active.png"),
      saved: require("../images/home/icon-nav-saved.png"),
      savedActive: require("../images/home/icon-nav-saved_active.png"),
    }

    var image = this.props.focus ? icon[this.props.name + 'Active'] : icon[this.props.name]
    return <Image style={{width: 26, height: 26}} source={image}/>;
  }
}

const TabStack =  TabNavigator({
  HOME: { screen: Home },
  EXPLORE: { screen: Explore },
  SUBMIT: { screen: Submit },
  SAVED: { screen: Saved },
  PROFILE: { screen: Profile },
}, {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        return <TabIcon focus={focused} name={routeName.toLowerCase()}/>;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#62a842',
      inactiveTintColor: '#000',
      labelStyle: {
        fontSize: 11,
        fontFamily: 'nunito-bold',
      },
      style: {
        backgroundColor: '#fff',
        borderTopWidth: 0, 
        elevation: 14,
        paddingTop:5,
        paddingBottom:6,
        height: 60,

        shadowColor: '#000',
        shadowOpacity: 0.15,
        shadowOffset: { width: 0, height: -1 },
        shadowRadius: 2,
      },
    },
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
    initialRouteName: 'HOME'
  });

export default class App extends React.Component {
  render() {
    return <TabStack />;
  }
}
